async function renderCard(
  offsetX,
  offsetY,
  doc,
  data,
  index,
  isInkSaver,
  isCutLines,
) {
  let offset = {
    x: offsetX,
    y: offsetY,
  };

  let sizeTitle = parseFloat(window.KeyValues["sizeTitle"]);
  let sizeBody = parseFloat(window.KeyValues["sizeBody"]);
  let sizeFocus = parseFloat(window.KeyValues["sizeFocus"]);
  let sizeMeta = parseFloat(window.KeyValues["sizeMeta"]);
  let sizeLink = parseFloat(window.KeyValues["sizeLink"]);

  let titleOptions = {
    lineGap: -6,
    characterSpacing: -0.15,
    width: card.width - margin * 2,
    align: "left",
  };

  let bodyOptions = {
    lineGap: 0,
    width: card.width - margin * 2,
    align: "left",
    height: card.height,
  };

  let linkOptions = {
    lineGap: 0,
    width: card.width - margin * 2,
    align: "center",
    characterSpacing: 0.2,
    height: 20,
  };

  var backgroundColor;

  if (index == 0) {
    // This is the special card
    backgroundColor = "#fff";

    doc.stroke();
    doc
      .rect(offset.x + bleed.x, offset.y + bleed.y, bleed.width, bleed.height)
      .strokeColor("black", 0.1)
      .stroke();

    doc.stroke();
    doc
      .rect(offset.x, offset.y, card.width, card.height)
      .strokeColor("black", 0.1)
      .stroke();

    const sectionMargin = 5;
    const sectionHeight = 42;
    const iconHeight = 24;
    const iconMarginH = 10;
    const iconMarginV = (sectionHeight - iconHeight) / 2;
    const iconWidth = iconHeight + iconMarginH * 2;
    Object.keys(Focus).forEach(async (focus, index) => {
      const frame = {
        x: offset.x + margin,
        y: offset.y + index * sectionHeight + index * sectionMargin + margin,
        w: card.width - margin * 2,
        h: sectionHeight,
      };

      doc
        .roundedRect(frame.x, frame.y, frame.w, frame.h, 3)
        .fillColor(window.KeyValues[Colors[focus]], 1)
        .fill();

      SVGtoPDF(
        doc,
        SVGStrings[focus],
        frame.x + iconMarginH,
        frame.y + iconMarginV,
        {
          preserveAspectRatio: "xMinYMin meet",
          width: iconHeight,
          height: iconHeight,
        },
      );

      doc
        .fontSize(window.KeyValues["sizeExplainerHeading"])
        .fillColor(ink, 1)
        .font(window.fontTitle)
        .text(Focus[focus], frame.x + iconWidth, frame.y + 3, {
          // characterSpacing: -0.05,
          width: frame.w - iconWidth,
          height: 2,
          align: "left",
        });

      let explanation = window.KeyValues["explainer_" + focus];
      doc
        .fontSize(window.KeyValues["sizeExplainerBody"])
        .lineGap(2)
        .font(window.fontBody)
        .text(explanation, frame.x + iconWidth, frame.y + 15, {
          lineGap: 0,
          width: frame.w - iconWidth - iconMarginH,
          align: "left",
          height: frame.h,
        });
    });

    let footerText = window.KeyValues["explainer_footer"];
    doc
      .fontSize(window.KeyValues["sizeExplainerBody"])
      .font(window.fontBody)
      .text(
        footerText,
        offset.x + margin,
        offset.y + 4 * sectionHeight + 4 * sectionMargin + margin + 6,
        {
          lineGap: 0,
          width: card.width - margin * 2,
          align: "left",
          continued: true,
          height: sectionHeight,
        },
      )
      .font(window.fontBodyBold)
      .text(window.KeyValues["prettyURL"], {
        link: data.link,
        underline: false,
        continued: false,
      });
  } else {
    // All the other cards
    backgroundColor = window.KeyValues[Colors[data.focus]];

    if (isInkSaver) {
      // background
      doc
        .rect(offset.x + bleed.x + 24, offset.y + bleed.y + 220, 58, 16)
        .fillColor(backgroundColor, 1)
        .fill();
    } else {
      doc
        .rect(offset.x + bleed.x, offset.y + bleed.y, bleed.width, bleed.height)
        .fillColor(backgroundColor, 1)
        .fill();
    }

    if (isCutLines) {
      doc.stroke();
      doc
        .rect(offset.x, offset.y, card.width, card.height)
        .strokeColor("black", 0.1)
        .stroke();
    }

    // What's the MVP?
    doc
      .fontSize(sizeTitle)
      .fillColor(ink, 1)
      .font(window.fontTitle)
      .text(
        data.title,
        offset.x + margin,
        offset.y +
          margin +
          i(0.45) -
          doc.heightOfString(data.title, titleOptions),
        titleOptions,
      );

    // Lorem ipsum dolor sit amet…
    let explanation = data.explanation;
    doc
      .fontSize(sizeBody)
      .lineGap(2)
      .font(window.fontBody)
      .text(
        explanation,
        offset.x + margin,
        offset.y + margin + i(0.55),
        bodyOptions,
      );

    // bottom section stroke
    let info = {
      x: offset.x + margin,
      y: offset.y + 162 - 5 + margin,
      w: card.width - margin * 2,
      h: 77 - margin * 2,
    };

    // Variables for the info block
    let ratio = 0.32;
    let lineHeight = 18;
    let topOffset = 10;

    SVGtoPDF(doc, SVGStrings[data.focus], info.x + 10, info.y + 6, {
      preserveAspectRatio: "xMinYMin meet",
      width: 30,
      height: 30,
    });

    // separator line
    doc
      .moveTo(info.x + info.w * ratio + 5, info.y)
      .lineTo(info.x + info.w * ratio + 5, info.y + info.h)
      .lineWidth(0.5)
      .strokeColor("black", 1)
      .stroke();

    // focus text
    let focusOptions = {
      lineGap: 0,
      characterSpacing: 0.15,
      width: info.w * ratio,
      height: 10,
      align: "center",
    };
    doc
      .fontSize(sizeFocus)
      .fillColor(ink, 1)
      .font(window.fontBodyBold)
      .text(
        Focus[data.focus],
        info.x,
        info.y - 7 + topOffset + lineHeight * 2,
        focusOptions,
      );

    // the 3 icons
    let dataOptions = {
      lineGap: 0,
      characterSpacing: 0.15,
      width: info.w * (1 - ratio),
      height: i(0.25),
      ellipse: true,
      align: "left",
    };

    SVGtoPDF(doc, SVGStrings[data.group], info.x + i(0.84), info.y, {
      preserveAspectRatio: "xMinYMin meet",
      width: 14,
      height: 14,
    });

    doc
      .fontSize(sizeMeta)
      .fillColor(ink, 1)
      .font(window.fontBodyBold)
      .text(
        data.group,
        info.x + info.w * ratio + 30,
        info.y - 7 + topOffset,
        dataOptions,
      );

    SVGtoPDF(doc, SVGStrings[data.props], info.x + i(0.84), info.y + 19, {
      preserveAspectRatio: "xMinYMin meet",
      width: 14,
      height: 14,
    });

    doc
      .fontSize(sizeMeta)
      .fillColor(ink, 1)
      .font(window.fontBodyBold)
      .text(
        data.props,
        info.x + info.w * ratio + 30,
        info.y - 7 + topOffset + lineHeight,
        dataOptions,
      );

    // Duration
    SVGtoPDF(doc, SVGStrings[data.time], info.x + i(0.84), info.y + 37, {
      preserveAspectRatio: "xMinYMin meet",
      width: 14,
      height: 14,
    });

    doc
      .fontSize(sizeMeta)
      .fillColor(ink, 1)
      .font(window.fontBodyBold)
      .text(
        data.time,
        info.x + info.w * ratio + 30,
        info.y - 7 + topOffset + lineHeight * 2,
        dataOptions,
      );

    // Shortlink
    doc
      .fontSize(sizeLink)
      .fillColor(ink, 1)
      .font(window.fontBodyBold)
      .text(
        "fc.cards/" + data.shortlink,
        info.x,
        info.y + info.h + 8,
        linkOptions,
      );
  }
}

async function renderBack(offsetX, offsetY, doc, data, index) {
  let offset = {
    x: offsetX,
    y: offsetY,
  };

  var backgroundColor;
  if (index == 0) {
    // This is the special card
    backgroundColor = "#fff";

    doc.stroke();
    doc
      .rect(offset.x + bleed.x, offset.y + bleed.y, bleed.width, bleed.height)
      .strokeColor("black", 0.1)
      .stroke();

    doc.stroke();
    doc
      .rect(offset.x, offset.y, card.width, card.height)
      .strokeColor("black", 0.1)
      .stroke();

    let offsetY = offset.y + margin;

    // SUPPLIES
    doc
      .fontSize(window.KeyValues["sizeSubtitle"])
      .fillColor(ink, 1)
      .font(window.fontTitle)
      .text(window.KeyValues["glossary_props"], offset.x + margin, offsetY, {
        lineGap: 0,
        characterSpacing: -0.15,
        width: card.width,
        height: 3,
        align: "left",
      });

    offsetY += window.KeyValues["sizeSubtitle"] * 2;

    var offsetX = offset.x + margin;
    const iconSize = 12;
    const colCount = 2;
    const rowHeight = 18;
    const rowWidth = (card.width - margin) / colCount;
    const sectionMargin = 5;
    const iconMarginRight = 4;

    Object.keys(dataPropUrls).forEach((key, index) => {
      let column = Math.floor(index / colCount);
      let row = index % colCount;

      SVGtoPDF(
        doc,
        SVGStrings[key],
        offsetX + row * rowWidth,
        offsetY + column * rowHeight,
        {
          preserveAspectRatio: "xMinYMin meet",
          width: iconSize,
          height: iconSize,
        },
      );

      doc
        .fontSize(window.KeyValues["sizeMeta"])
        .fillColor(ink, 1)
        .font(window.fontBody)
        .text(
          key,
          offsetX + row * rowWidth + iconSize + iconMarginRight,
          offsetY + column * rowHeight,
          {
            lineGap: 0,
            characterSpacing: -0.15,
            width: card.width,
            height: 3,
            align: "left",
          },
        );
    });
    offsetY +=
      Math.floor(Object.keys(dataPropUrls).length / colCount) * rowHeight;
    offsetY += sectionMargin;

    // GROUP SIZE
    doc
      .fontSize(window.KeyValues["sizeSubtitle"])
      .fillColor(ink, 1)
      .font(window.fontTitle)
      .text(window.KeyValues["glossary_size"], offset.x + margin, offsetY, {
        lineGap: 0,
        characterSpacing: -0.15,
        width: card.width,
        height: 3,
        align: "left",
      });

    offsetY += window.KeyValues["sizeSubtitle"] * 2;

    Object.keys(GroupSizeURLs).forEach((key, index) => {
      let column = Math.floor(index / colCount);
      let row = index % colCount;

      SVGtoPDF(
        doc,
        SVGStrings[key],
        offsetX + row * rowWidth,
        offsetY + column * rowHeight,
        {
          preserveAspectRatio: "xMinYMin meet",
          width: iconSize,
          height: iconSize,
        },
      );

      doc
        .fontSize(window.KeyValues["sizeMeta"])
        .fillColor(ink, 1)
        .font(window.fontBody)
        .text(
          key,
          offsetX + row * rowWidth + iconSize + iconMarginRight,
          offsetY + column * rowHeight,
          {
            lineGap: 0,
            characterSpacing: -0.15,
            width: card.width,
            height: 3,
            align: "left",
          },
        );
    });
    offsetY +=
      Math.floor(Object.keys(GroupSizeURLs).length / colCount) * rowHeight;
    offsetY += sectionMargin;

    // TIME NEEDED
    doc
      .fontSize(window.KeyValues["sizeSubtitle"])
      .fillColor(ink, 1)
      .font(window.fontTitle)
      .text(window.KeyValues["glossary_time"], offset.x + margin, offsetY, {
        lineGap: 0,
        characterSpacing: -0.15,
        width: card.width,
        height: 3,
        align: "left",
      });

    offsetY += window.KeyValues["sizeSubtitle"] * 2;

    Object.keys(DurationURLs).forEach((key, index) => {
      // console.log(key, DurationURLs[key]);

      let column = Math.floor(index / colCount);
      let row = index % colCount;

      SVGtoPDF(
        doc,
        SVGStrings[key],
        offsetX + row * rowWidth,
        offsetY + column * rowHeight,
        {
          preserveAspectRatio: "xMinYMin meet",
          width: iconSize,
          height: iconSize,
        },
      );

      doc
        .fontSize(window.KeyValues["sizeMeta"])
        .fillColor(ink, 1)
        .font(window.fontBody)
        .text(
          key,
          offsetX + row * rowWidth + iconSize + iconMarginRight,
          offsetY + column * rowHeight,
          {
            lineGap: 0,
            characterSpacing: -0.15,
            width: card.width,
            height: 3,
            align: "left",
          },
        );
    });
    offsetY +=
      Math.floor(Object.keys(GroupSizeURLs).length / colCount) * rowHeight;
  } else {
    // All the other cards
    backgroundColor = window.KeyValues[Colors[data.focus]];

    // background
    doc
      .rect(offset.x + bleed.x, offset.y + bleed.y, bleed.width, 72)
      .fillColor(backgroundColor, 1)
      .fill();

    doc.stroke();
    doc
      .rect(offset.x, offset.y, card.width, card.height)
      .strokeColor("black", 0.1)
      .stroke();

    let titleOptions = {
      lineGap: -3,
      characterSpacing: -0.15,
      width: card.width - margin * 3,
      align: "left",
    };

    // What's the MVP?
    doc
      .fontSize(15)
      .fillColor(ink, 1)
      .font(window.fontTitle)
      .text(
        data.title,
        offset.x + margin,
        offset.y +
          margin +
          i(0.48) -
          doc.heightOfString(data.title, titleOptions),
        titleOptions,
      );

    // bottom section stroke
    let info = {
      x: offset.x + card.width - 20,
      y: offset.y + 6,
    };

    const iconSize = 12;

    // Group size
    SVGtoPDF(doc, SVGStrings[data.group], info.x, info.y, {
      preserveAspectRatio: "xMinYMin meet",
      width: iconSize,
      height: iconSize,
    });

    // Props required
    SVGtoPDF(doc, SVGStrings[data.props], info.x, info.y + 15, {
      preserveAspectRatio: "xMinYMin meet",
      width: iconSize,
      height: iconSize,
    });

    // Duration
    SVGtoPDF(doc, SVGStrings[data.time], info.x, info.y + 30, {
      preserveAspectRatio: "xMinYMin meet",
      width: iconSize,
      height: iconSize,
    });

    doc
      .fontSize(window.KeyValues["sizeWetErase"])
      .lineGap(2)
      .font(window.fontBody)
      .text(
        window.KeyValues["wet_erase"],
        offset.x,
        offset.y + card.height - margin - window.KeyValues["sizeWetErase"],
        {
          lineGap: 0,
          width: card.width,
          align: "center",
          height: 3,
        },
      );
  }
}
