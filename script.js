var Airtable = require("airtable");
var base = new Airtable({
  apiKey:
    "patkhXt2cWQiefO2I.3d6b7ca0d4bbd5b3ebaa14d40cf85186378c3f0bd23ad1b9dcc83262943485bb",
}).base("appNz9ctqtljWTURw");

const i = (inch) => inch * 72;
const _ = (query) => document.querySelector(query);

const card = {
  width: i(2.5),
  height: i(3.5),
};

const margin = i(0.18);
const ink = "#000";

const bleedMargin = i(0.2);
const bleed = {
  x: -bleedMargin,
  y: -bleedMargin,
  width: card.width + bleedMargin * 2,
  height: card.height + bleedMargin * 2,
};

_("#layout").onchange = () => {
  let urlparams = new URLSearchParams(window.location.search).get("cardID");
  let selectedCard = urlparams ? urlparams : 0;
  renderPDF(selectedCard);
};

_("#inksaver").onchange = () => {
  let urlparams = new URLSearchParams(window.location.search).get("cardID");
  let selectedCard = urlparams ? urlparams : 0;
  renderPDF(selectedCard);
};

var Store = base("Property").select({
  fields: ["Key", "Value"],
  pageSize: 100,
});

// Sorting example
var Records = base("Cards V3").select({
  fields: [
    "Focus",
    "Title",
    "Group",
    "Props Set Up",
    "Time",
    "Explanation",
    "Short link",
  ],
  pageSize: 100,
  sort: [
    {
      field: "Focus",
      direction: "asc",
    },
    {
      field: "Title",
      direction: "asc",
    },
  ],
});

const Colors = {
  brainstorm: "colorGreen",
  connect: "colorBlue",
  clarify: "colorYellow",
  decide: "colorRed",
};

const emoji = {
  brainstorm: "🟢",
  connect: "🔵",
  clarify: "🟡",
  decide: "🔴",
};

const Focus = {
  connect: "Connect",
  brainstorm: "Brainstorm",
  clarify: "Clarify",
  decide: "Decide",
};

const IconURLs = {
  brainstorm: "img/focus_ideation.svg",
  connect: "img/focus_emotion.svg",
  clarify: "img/focus_clarification.svg",
  decide: "img/focus_execution.svg",
};

const GroupSizeURLs = {
  Solo: "img/size_individuals.svg",
  Pairs: "img/size_pairs.svg",
  Groups: "img/size_small_groups.svg",
  Everybody: "img/size_everybody.svg",
};

const DurationURLs = {
  "2 Minutes": "img/2min.svg",
  "5 Minutes": "img/5min.svg",
  "10 Minutes": "img/10min.svg",
  "15 Minutes": "img/15min.svg",
  "20+ Minutes": "img/20min.svg",
};

const dataPropUrls = {
  "Flipchart & Stickies": "img/tool_flipchart_stickies.svg",
  "Paper & Markers": "img/tool_pen_paper.svg",
  "Flipchart & Markers": "img/tool_flipchart_marker.svg",
  "Pen & Paper": "img/tool_pen_paper.svg",
  "Stickies & Markers": "img/tool_stickies.svg",
  "Index Cards": "img/tool_pen_indexcards.svg",
  "Prepared Materials": "img/tool_special.svg",
  "No Supplies": "img/tool_none.svg",
};

// var SVGStrings = {};

//
const SVGStrings = {
  "Pen & Paper":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<g clip-path="url(#clip0_21_1888)">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M16 28H9.5C8.11929 28 7 26.8807 7 25.5V19V17.6765C7 16.2958 8.11929 15.1765 9.5 15.1765H12.7C14.633 15.1765 16.2 13.6095 16.2 11.6765V8.5C16.2 7.11929 17.3193 6 18.7 6H20H23.5C24.8807 6 26 7.11929 26 8.5V15.5L28 13.5V8.5C28 6.01472 25.9853 4 23.5 4H20H18.7H16.5C15.1606 4 14.1334 4.3361 13.4278 4.68891C13.0766 4.8645 12.807 5.04316 12.6187 5.18438C12.5245 5.25504 12.4504 5.31655 12.3962 5.36396C12.3691 5.38768 12.3469 5.40791 12.3297 5.42403L12.3076 5.44508L12.2993 5.45317L12.2959 5.45658L12.2943 5.45813L12.2936 5.45886C12.2932 5.45921 12.2929 5.45955 12.9663 6.13295L12.2929 5.45956L6.48053 11.2719C6.46188 11.2889 6.4394 11.31 6.41368 11.3354C6.36057 11.3879 6.29353 11.4588 6.21762 11.5495C6.06565 11.731 5.8785 11.9912 5.69675 12.3395C5.3304 13.0417 5 14.0754 5 15.5V17.6765V19V25.5C5 27.9853 7.01472 30 9.5 30H16V28ZM9.5 13.1765C8.68212 13.1765 7.9152 13.3947 7.25428 13.776C7.32319 13.571 7.3985 13.4015 7.46992 13.2647C7.57983 13.054 7.68435 12.9131 7.75113 12.8333C7.7846 12.7934 7.80871 12.7686 7.8194 12.758L7.82123 12.7562L7.8417 12.7392L7.87377 12.7071L13.6998 6.88104L13.7132 6.86911C13.7319 6.85272 13.7671 6.82309 13.8188 6.78438C13.9221 6.70685 14.0901 6.59384 14.3222 6.47776C14.4545 6.41161 14.6084 6.34408 14.784 6.28142C14.4123 6.93616 14.2 7.69329 14.2 8.5V11.6765C14.2 12.5049 13.5284 13.1765 12.7 13.1765H9.5Z" fill="black"/>\n<path d="M18.0439 26.3672L29.7263 14.6839C30.7753 13.6349 32.6535 13.8124 33.9214 15.0805C35.1894 16.3485 35.3638 18.2238 34.3148 19.2728L22.6325 30.9561C22.6044 30.9842 22.5662 31 22.5264 31L18.15 31C18.0672 31 18 30.9328 18 30.85V26.4732C18 26.4334 18.0158 26.3953 18.0439 26.3672Z" fill="white" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M25.6658 27.9757C24.8 28.8415 22.8439 28.7227 21.6006 27.4794C20.3573 26.236 20.2385 24.28 21.1043 23.4142" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</g>\n<defs>\n<clipPath id="clip0_21_1888">\n<rect width="36" height="36" fill="white"/>\n</clipPath>\n</defs>\n</svg>\n',
  brainstorm:
    '<svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M48.0573 49C54.3349 45.008 58.5 37.9904 58.5 30C58.5 17.5736 48.4264 7.5 36 7.5C23.5736 7.5 13.5 17.5736 13.5 30C13.5 37.9904 17.6651 45.008 23.9427 49H48.0573Z" fill="white"/>\n<mask id="path-2-inside-1_401_1163" fill="white">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M48 52.5196C48 51.4487 48.5795 50.4711 49.4654 49.8695C55.8232 45.5525 60 38.264 60 30C60 16.7452 49.2548 6 36 6C22.7452 6 12 16.7452 12 30C12 38.264 16.1768 45.5525 22.5346 49.8695C23.4205 50.4711 24 51.4487 24 52.5196V57C24 63.6274 29.3726 69 36 69C42.6274 69 48 63.6274 48 57V52.5196Z"/>\n</mask>\n<path d="M22.5346 49.8695L20.8493 52.3514L22.5346 49.8695ZM49.4654 49.8695L51.1507 52.3514L49.4654 49.8695ZM57 30C57 37.2283 53.3507 43.6051 47.7802 47.3876L51.1507 52.3514C58.2957 47.4998 63 39.2998 63 30H57ZM36 9C47.598 9 57 18.402 57 30H63C63 15.0883 50.9117 3 36 3V9ZM15 30C15 18.402 24.402 9 36 9V3C21.0883 3 9 15.0883 9 30H15ZM24.2198 47.3876C18.6493 43.6051 15 37.2283 15 30H9C9 39.2998 13.7043 47.4998 20.8493 52.3514L24.2198 47.3876ZM27 57V52.5196H21V57H27ZM36 66C31.0294 66 27 61.9706 27 57H21C21 65.2843 27.7157 72 36 72V66ZM45 57C45 61.9706 40.9706 66 36 66V72C44.2843 72 51 65.2843 51 57H45ZM45 52.5196V57H51V52.5196H45ZM20.8493 52.3514C20.9222 52.4009 20.9655 52.455 20.9861 52.4917C21.0046 52.5244 21 52.5324 21 52.5196H27C27 50.3393 25.8279 48.4794 24.2198 47.3876L20.8493 52.3514ZM47.7802 47.3876C46.1721 48.4794 45 50.3393 45 52.5196H51C51 52.5324 50.9954 52.5244 51.0139 52.4917C51.0345 52.455 51.0778 52.4009 51.1507 52.3514L47.7802 47.3876Z" fill="black" mask="url(#path-2-inside-1_401_1163)"/>\n<rect x="24" y="49" width="24" height="3" fill="black"/>\n<path d="M22.5766 37.75C21.2162 35.3937 20.5 32.7208 20.5 30C20.5 27.2792 21.2162 24.6063 22.5766 22.25C23.937 19.8937 25.8937 17.937 28.25 16.5766C30.6063 15.2162 33.2792 14.5 36 14.5" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>\n</svg>\n',
  connect:
    '<svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M24 38H9.5L36 65L62.5 38H50L43.5 27L37 38H32L28 32L24 38Z" fill="white"/>\n<mask id="path-2-inside-1_401_1157" fill="white">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M47.8597 54.1433L38.3838 63.6192C37.0672 64.9359 34.9324 64.9359 33.6157 63.6192L9.77487 39.7784C3.19142 33.1949 3.19142 22.5211 9.77487 15.9376C16.3583 9.35414 27.0322 9.35414 33.6157 15.9376L35.9999 18.3218L38.3841 15.9376C44.9676 9.35414 55.6415 9.35414 62.225 15.9376C68.8084 22.5211 68.8084 33.1949 62.225 39.7784L47.9205 54.0829C47.9003 54.1031 47.88 54.1232 47.8597 54.1433Z"/>\n</mask>\n<path d="M47.8597 54.1433L45.7491 52.0114C45.7455 52.0149 45.742 52.0185 45.7384 52.022L47.8597 54.1433ZM38.3838 63.6192L36.2625 61.4979L36.2625 61.4979L38.3838 63.6192ZM33.6157 63.6192L35.737 61.4979L35.737 61.4979L33.6157 63.6192ZM9.77487 39.7784L7.65355 41.8997L9.77487 39.7784ZM9.77487 15.9376L11.8962 18.0589L11.8962 18.0589L9.77487 15.9376ZM33.6157 15.9376L31.4944 18.0589L31.4944 18.0589L33.6157 15.9376ZM35.9999 18.3218L33.8786 20.4431C35.0502 21.6147 36.9497 21.6147 38.1212 20.4431L35.9999 18.3218ZM38.3841 15.9376L36.2628 13.8163L36.2628 13.8163L38.3841 15.9376ZM62.225 39.7784L60.1036 37.6571L60.1036 37.6571L62.225 39.7784ZM47.9205 54.0829L50.0418 56.2042L50.0418 56.2042L47.9205 54.0829ZM45.7384 52.022L36.2625 61.4979L40.5052 65.7405L49.9811 56.2646L45.7384 52.022ZM36.2625 61.4979C36.1174 61.643 35.8821 61.643 35.737 61.4979L31.4944 65.7405C33.9826 68.2288 38.0169 68.2288 40.5052 65.7405L36.2625 61.4979ZM35.737 61.4979L11.8962 37.6571L7.65355 41.8997L31.4944 65.7405L35.737 61.4979ZM11.8962 37.6571C6.48431 32.2452 6.48431 23.4708 11.8962 18.0589L7.65355 13.8163C-0.101477 21.5713 -0.101478 34.1447 7.65355 41.8997L11.8962 37.6571ZM11.8962 18.0589C17.3081 12.647 26.0825 12.647 31.4944 18.0589L35.737 13.8163C27.982 6.06124 15.4086 6.06124 7.65355 13.8163L11.8962 18.0589ZM31.4944 18.0589L33.8786 20.4431L38.1212 16.2005L35.737 13.8163L31.4944 18.0589ZM38.1212 20.4431L40.5055 18.0589L36.2628 13.8163L33.8786 16.2005L38.1212 20.4431ZM40.5055 18.0589C45.9173 12.647 54.6918 12.647 60.1036 18.0589L64.3463 13.8163C56.5912 6.06124 44.0179 6.06124 36.2628 13.8163L40.5055 18.0589ZM60.1036 18.0589C65.5155 23.4708 65.5155 32.2452 60.1036 37.6571L64.3463 41.8997C72.1013 34.1447 72.1013 21.5713 64.3463 13.8163L60.1036 18.0589ZM60.1036 37.6571L45.7992 51.9616L50.0418 56.2042L64.3463 41.8997L60.1036 37.6571ZM45.7992 51.9616C45.7824 51.9783 45.7657 51.9949 45.7491 52.0114L49.9704 56.2752C49.9943 56.2516 50.0181 56.2279 50.0418 56.2042L45.7992 51.9616Z" fill="black" mask="url(#path-2-inside-1_401_1157)"/>\n<path d="M11 38H24.6792L28.4528 32L32.2264 38H36.9434L43.0755 27L49.2075 38H61" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>\n</svg>\n',
  clarify:
    '<svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M63 27L52.2 12H19.8L9 27H36H63Z" fill="white"/>\n<path d="M63 27L52.2 12H19.8L9 27L36 63L63 27Z" stroke="black" stroke-width="3" stroke-linejoin="round"/>\n<path d="M63 27C50.4 27 39.6 27 27 27M9 27H27M27 27V12M27 27V51" stroke="black" stroke-width="3" stroke-linejoin="round"/>\n</svg>\n',
  decide:
    '<svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M14.5512 27L20.75 9H30.75L19.4099 39L14.5512 27Z" fill="white"/>\n<path d="M20.75 9L19.3317 8.51159C19.5402 7.90629 20.1098 7.5 20.75 7.5V9ZM14.5512 27L13.1609 27.5629C13.025 27.2273 13.0151 26.8539 13.133 26.5116L14.5512 27ZM32.75 39V37.5C33.2221 37.5 33.6667 37.7223 33.95 38.1C34.2333 38.4777 34.3222 38.9667 34.19 39.42L32.75 39ZM25.75 63L26.8405 64.0299C26.3647 64.5338 25.6056 64.646 25.0043 64.3015C24.4029 63.957 24.116 63.2453 24.31 62.58L25.75 63ZM59.75 27V25.5C60.3489 25.5 60.8904 25.8563 61.1275 26.4062C61.3645 26.9562 61.2517 27.5945 60.8405 28.0299L59.75 27ZM40.25 27V28.5C39.7485 28.5 39.2801 28.2493 39.0019 27.8321C38.7237 27.4148 38.6725 26.886 38.8654 26.4231L40.25 27ZM47.75 9V7.5C48.2515 7.5 48.7199 7.75065 48.9981 8.16795C49.2763 8.58525 49.3275 9.11397 49.1346 9.57692L47.75 9ZM19.4099 39V40.5H18.3989L18.0195 39.5629L19.4099 39ZM22.1683 9.48841L15.9695 27.4884L13.133 26.5116L19.3317 8.51159L22.1683 9.48841ZM34.19 39.42L27.19 63.42L24.31 62.58L31.31 38.58L34.19 39.42ZM24.6595 61.9701L58.6595 25.9701L60.8405 28.0299L26.8405 64.0299L24.6595 61.9701ZM59.75 28.5H40.25V25.5H59.75V28.5ZM38.8654 26.4231L46.3654 8.42308L49.1346 9.57692L41.6346 27.5769L38.8654 26.4231ZM15.9416 26.4371L20.8002 38.4371L18.0195 39.5629L13.1609 27.5629L15.9416 26.4371ZM19.4099 37.5H32.75V40.5H19.4099V37.5ZM47.75 10.5H30.75V7.5H47.75V10.5ZM30.75 10.5H20.75V7.5H30.75V10.5ZM18.0068 38.4696L29.3469 8.46962L32.1531 9.53038L20.813 39.5304L18.0068 38.4696Z" fill="black"/>\n</svg>\n',
  Solo: '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M25.2294 6.01102C28.4091 7.92838 30.6969 11.0303 31.5895 14.6345C32.482 18.2386 31.9063 22.0497 29.989 25.2294C28.0716 28.4091 24.9697 30.6969 21.3655 31.5895C17.7614 32.482 13.9503 31.9063 10.7706 29.989C7.59091 28.0716 5.30314 24.9697 4.41055 21.3655C3.51795 17.7614 4.09366 13.9503 6.01102 10.7706C7.92838 7.59091 11.0303 5.30314 14.6345 4.41055C18.2386 3.51795 22.0497 4.09366 25.2294 6.01102L25.2294 6.01102Z" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  Everybody:
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<circle cx="18.001" cy="6.29998" r="2.7" fill="black"/>\n<circle cx="18.001" cy="29.7" r="2.7" fill="black"/>\n<circle cx="25.201" cy="8.10002" r="2.7" fill="black"/>\n<circle cx="25.201" cy="27.9" r="2.7" fill="black"/>\n<circle cx="10.8011" cy="8.10002" r="2.7" fill="black"/>\n<circle cx="10.8011" cy="27.9" r="2.7" fill="black"/>\n<circle cx="29.701" cy="13.9421" r="2.7" fill="black"/>\n<circle cx="6.31951" cy="13.9421" r="2.7" fill="black"/>\n<circle cx="29.701" cy="22.0309" r="2.7" fill="black"/>\n<circle cx="6.31951" cy="22.0309" r="2.7" fill="black"/>\n</svg>\n',
  Pairs:
    '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 36 36">\n  <mask id="a" fill="#fff">\n    <path fill-rule="evenodd" d="M19.5 10.5c.2.5.8.8 1.3.7.5 0 .9-.6.7-1.1A9 9 0 1 0 11 21.8c.5.1 1-.3 1-.9.1-.5-.3-1-.8-1.1a7 7 0 1 1 8.3-9.3ZM30 23a7 7 0 1 1-14 0 7 7 0 0 1 14 0Zm2 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" clip-rule="evenodd"/>\n  </mask>\n  <path fill="#000" d="m20.8 11.2.4 2-.4-2Zm-1.3-.7-1.8.7 1.8-.7Zm2-.4 2-.7-2 .7ZM11 21.8l.4-2-.4 2Zm1-.9 2 .2-2-.2Zm-.8-1.1.5-2-.5 2Zm9.2-10.5c.5-.1.9.2 1 .5l-3.7 1.4c.5 1.4 2 2.3 3.5 2l-.8-4Zm-.8 1.4c-.1-.4 0-.8.2-1 .1-.2.3-.4.6-.4l.8 3.9c1.4-.3 2.9-1.8 2.2-3.8l-3.8 1.3ZM13 6c3 0 5.7 2 6.6 4.7l3.8-1.3A11 11 0 0 0 13 2v4Zm-7 7a7 7 0 0 1 7-7V2C7 2 2 7 2 13h4Zm5.4 6.8A7 7 0 0 1 6 13H2a11 11 0 0 0 8.6 10.7l.8-3.9Zm-1.3 1c0-.3.1-.6.3-.7.2-.2.6-.4 1-.3l-.8 4c2 .4 3.3-1.3 3.4-2.7l-4-.4Zm.6 1a.9.9 0 0 1-.6-1l4 .3c.1-1.6-1-2.9-2.4-3.3l-1 4ZM4 13a9 9 0 0 0 6.7 8.7l1-3.9A5 5 0 0 1 8 13H4Zm9-9c-5 0-9 4-9 9h4a5 5 0 0 1 5-5V4Zm8.4 5.8A9 9 0 0 0 13 4v4a5 5 0 0 1 4.7 3.2l3.7-1.4ZM23 32c5 0 9-4 9-9h-4a5 5 0 0 1-5 5v4Zm-9-9c0 5 4 9 9 9v-4a5 5 0 0 1-5-5h-4Zm9-9c-5 0-9 4-9 9h4a5 5 0 0 1 5-5v-4Zm9 9c0-5-4-9-9-9v4a5 5 0 0 1 5 5h4Zm-9 11c6 0 11-5 11-11h-4a7 7 0 0 1-7 7v4ZM12 23c0 6 5 11 11 11v-4a7 7 0 0 1-7-7h-4Zm11-11c-6 0-11 5-11 11h4a7 7 0 0 1 7-7v-4Zm11 11c0-6-5-11-11-11v4a7 7 0 0 1 7 7h4Z" mask="url(#a)"/>\n</svg>\n',
  Groups:
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<circle cx="8" cy="16" r="4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<circle cx="12" cy="28" r="4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<circle cx="24" cy="28" r="4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<circle cx="18" cy="7" r="4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<circle cx="28" cy="16" r="4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "20+ Minutes":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M18 4C20.4522 4 22.8614 4.64411 24.9865 5.86785C27.1115 7.09159 28.8779 8.85204 30.1087 10.973C31.3396 13.0939 31.9917 15.501 31.9999 17.9532C32.0081 20.4054 31.3721 22.8167 30.1554 24.9459L18 18L18 4Z" fill="white"/>\n<path d="M18 18V4" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M18 18L30 25" stroke="black" stroke-width="2" stroke-dasharray="4 2"/>\n<path d="M27.5176 28.2672C24.9249 30.6706 21.5188 32.0042 17.9835 32C14.4481 31.9958 11.0452 30.6542 8.45816 28.2447C5.87111 25.8351 4.29148 22.536 4.03647 19.0098C3.78146 15.4837 4.86996 11.9916 7.08338 9.23487C9.29681 6.47814 12.4712 4.6609 15.9692 4.14807C19.4672 3.63525 23.0297 4.46481 25.9412 6.47019C28.8528 8.47556 30.8979 11.5082 31.6658 14.9592C32.4337 18.4102 31.8675 22.0239 30.081 25.0746" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "2 Minutes":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M25.2294 6.01102C28.2031 7.80417 30.4043 10.6384 31.4055 13.9635C32.4067 17.2885 32.1364 20.8669 30.6473 24.0039C29.1581 27.1408 26.5563 29.6124 23.347 30.9387C20.1378 32.2649 16.5502 32.3512 13.2809 31.1807C10.0117 30.0102 7.29406 27.6665 5.65587 24.6047C4.01767 21.5429 3.57579 17.9816 4.41605 14.6123C5.25631 11.243 7.31872 8.30627 10.2028 6.37229C13.0869 4.43831 16.5868 3.64518 20.0228 4.1469" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M18 4C19.2235 4 20.4417 4.16038 21.6235 4.47704L18 18L18 4Z" fill="white" stroke="black" stroke-width="2" stroke-linejoin="round"/>\n</svg>\n',
  "15 Minutes":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M18 4C19.8385 4 21.659 4.36212 23.3576 5.06569C25.0561 5.76925 26.5995 6.80049 27.8995 8.10051C29.1995 9.40053 30.2307 10.9439 30.9343 12.6424C31.6379 14.341 32 16.1615 32 18L18 18L18 4Z" fill="white" stroke="black" stroke-width="2" stroke-linejoin="round"/>\n<path d="M31.4717 21.8098C30.5267 25.1512 28.3737 28.0222 25.4306 29.8653C22.4876 31.7084 18.9647 32.3919 15.546 31.7832C12.1273 31.1746 9.05676 29.3171 6.93073 26.5715C4.80469 23.826 3.77487 20.3883 4.04125 16.926C4.30764 13.4638 5.85121 10.2241 8.37222 7.83606C10.8932 5.44803 14.2117 4.08212 17.6833 4.00358C21.1549 3.92504 24.5318 5.13947 27.1583 7.41103C29.7847 9.68259 31.4732 12.8492 31.8959 16.2958" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "Flipchart & Markers":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<g clip-path="url(#clip0_9_88)">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M20.5 4H9.5C8.11929 4 7 5.11929 7 6.5V16V20.5C7 21.8807 8.11929 23 9.5 23H15H16V25V26V30C16 30.5523 15.5523 31 15 31C14.4477 31 14 30.5523 14 30V25H9.5C8.57493 25 7.71505 24.7209 7 24.2422V33C7 33.5523 6.55228 34 6 34C5.44772 34 5 33.5523 5 33V20.5V16V6.5C5 4.01472 7.01472 2 9.5 2H20.5C22.9853 2 25 4.01472 25 6.5V9.5L23 11.5V6.5C23 5.11929 21.8807 4 20.5 4ZM25 24.5L24.0858 25.4142C23.789 25.711 23.4086 25.9048 23 25.9729V33C23 33.5523 23.4477 34 24 34C24.5523 34 25 33.5523 25 33V24.5ZM9 7C9 6.44772 9.44772 6 10 6H19C19.5523 6 20 6.44772 20 7C20 7.55228 19.5523 8 19 8H10C9.44771 8 9 7.55228 9 7ZM10 9C9.44772 9 9 9.44772 9 10C9 10.5523 9.44772 11 10 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H10Z" fill="black"/>\n<path d="M18.0439 19.3672L29.7263 7.68393C30.7753 6.63489 32.6535 6.81244 33.9214 8.08049C35.1894 9.34854 35.3638 11.2238 34.3148 12.2728L22.6325 23.9561C22.6044 23.9842 22.5662 24 22.5264 24L18.15 24C18.0672 24 18 23.9328 18 23.85V19.4732C18 19.4334 18.0158 19.3953 18.0439 19.3672Z" fill="white" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M25.6658 20.9757C24.8 21.8415 22.8439 21.7227 21.6006 20.4794C20.3573 19.236 20.2385 17.28 21.1043 16.4142" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</g>\n<defs>\n<clipPath id="clip0_9_88">\n<rect width="36" height="36" fill="white"/>\n</clipPath>\n</defs>\n</svg>\n',
  "Flipchart & Stickies":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M20.5 4H9.5C8.11929 4 7 5.11929 7 6.5V16V20.5C7 21.8807 8.11929 23 9.5 23H15H16V25V30C16 30.5523 15.5523 31 15 31C14.4477 31 14 30.5523 14 30V25H9.5C8.57493 25 7.71505 24.7209 7 24.2422V33C7 33.5523 6.55228 34 6 34C5.44772 34 5 33.5523 5 33V20.5V16V6.5C5 4.01472 7.01472 2 9.5 2H20.5C22.9853 2 25 4.01472 25 6.5V10H23V6.5C23 5.11929 21.8807 4 20.5 4ZM25 29H23V33C23 33.5523 23.4477 34 24 34C24.5523 34 25 33.5523 25 33V29ZM9 7C9 6.44772 9.44772 6 10 6H19C19.5523 6 20 6.44772 20 7C20 7.55228 19.5523 8 19 8H10C9.44771 8 9 7.55228 9 7ZM10 9C9.44772 9 9 9.44772 9 10C9 10.5523 9.44772 11 10 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H10Z" fill="black"/>\n<path d="M23 27H25.8572C27.5714 27 28.2381 26.2857 28.2381 26.2857L32.0476 22.4762C32.0476 22.4762 33 21.7143 33 19.8571V17" stroke="black" stroke-width="2"/>\n<path d="M20 12L31 12C32.1046 12 33 12.8954 33 14V18.8182C33 19.9228 32.1046 20.8182 31 20.8182H28.3182C27.4898 20.8182 26.8182 21.4898 26.8182 22.3182V25C26.8182 26.1046 25.9228 27 24.8182 27H20C18.8954 27 18 26.1046 18 25V14C18 12.8954 18.8954 12 20 12Z" fill="white" stroke="black" stroke-width="2"/>\n</svg>\n',
  "Paper & Markers":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<g clip-path="url(#clip0_21_1888)">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M16 28H9.5C8.11929 28 7 26.8807 7 25.5V19V17.6765C7 16.2958 8.11929 15.1765 9.5 15.1765H12.7C14.633 15.1765 16.2 13.6095 16.2 11.6765V8.5C16.2 7.11929 17.3193 6 18.7 6H20H23.5C24.8807 6 26 7.11929 26 8.5V15.5L28 13.5V8.5C28 6.01472 25.9853 4 23.5 4H20H18.7H16.5C15.1606 4 14.1334 4.3361 13.4278 4.68891C13.0766 4.8645 12.807 5.04316 12.6187 5.18438C12.5245 5.25504 12.4504 5.31655 12.3962 5.36396C12.3691 5.38768 12.3469 5.40791 12.3297 5.42403L12.3076 5.44508L12.2993 5.45317L12.2959 5.45658L12.2943 5.45813L12.2936 5.45886C12.2932 5.45921 12.2929 5.45955 12.9663 6.13295L12.2929 5.45956L6.48053 11.2719C6.46188 11.2889 6.4394 11.31 6.41368 11.3354C6.36057 11.3879 6.29353 11.4588 6.21762 11.5495C6.06565 11.731 5.8785 11.9912 5.69675 12.3395C5.3304 13.0417 5 14.0754 5 15.5V17.6765V19V25.5C5 27.9853 7.01472 30 9.5 30H16V28ZM9.5 13.1765C8.68212 13.1765 7.9152 13.3947 7.25428 13.776C7.32319 13.571 7.3985 13.4015 7.46992 13.2647C7.57983 13.054 7.68435 12.9131 7.75113 12.8333C7.7846 12.7934 7.80871 12.7686 7.8194 12.758L7.82123 12.7562L7.8417 12.7392L7.87377 12.7071L13.6998 6.88104L13.7132 6.86911C13.7319 6.85272 13.7671 6.82309 13.8188 6.78438C13.9221 6.70685 14.0901 6.59384 14.3222 6.47776C14.4545 6.41161 14.6084 6.34408 14.784 6.28142C14.4123 6.93616 14.2 7.69329 14.2 8.5V11.6765C14.2 12.5049 13.5284 13.1765 12.7 13.1765H9.5Z" fill="black"/>\n<path d="M18.0439 26.3672L29.7263 14.6839C30.7753 13.6349 32.6535 13.8124 33.9214 15.0805C35.1894 16.3485 35.3638 18.2238 34.3148 19.2728L22.6325 30.9561C22.6044 30.9842 22.5662 31 22.5264 31L18.15 31C18.0672 31 18 30.9328 18 30.85V26.4732C18 26.4334 18.0158 26.3953 18.0439 26.3672Z" fill="white" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M25.6658 27.9757C24.8 28.8415 22.8439 28.7227 21.6006 27.4794C20.3573 26.236 20.2385 24.28 21.1043 23.4142" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</g>\n<defs>\n<clipPath id="clip0_21_1888">\n<rect width="36" height="36" fill="white"/>\n</clipPath>\n</defs>\n</svg>\n',
  "5 Minutes":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M18 4C20.4399 4 22.8375 4.63768 24.9551 5.84982L18 18L18 4Z" fill="white" stroke="black" stroke-width="2" stroke-linejoin="round"/>\n<path d="M27.8995 8.1005C30.3549 10.5559 31.8102 13.8362 31.9827 17.3044C32.1552 20.7726 31.0327 24.1812 28.8331 26.8682C26.6335 29.5551 23.5138 31.3288 20.0798 31.8446C16.6459 32.3605 13.1428 31.5818 10.2507 29.6597C7.35873 27.7376 5.28423 24.8094 4.43011 21.4436C3.57599 18.0778 4.0032 14.5146 5.62878 11.4461C7.25436 8.37766 9.9623 6.02279 13.2267 4.83885C16.4911 3.65491 20.0791 3.7264 23.2937 5.03943" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "10 Minutes":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M18 4C20.4472 4 22.8516 4.64147 24.9736 5.86045C27.0956 7.07944 28.861 8.83335 30.0938 10.9473L18 18L18 4Z" fill="white" stroke="black" stroke-width="2" stroke-linejoin="round"/>\n<path d="M31.5961 14.6614C32.4242 18.0336 31.9695 21.5934 30.3202 24.6492C28.671 27.705 25.945 30.0389 22.6715 31.1976C19.398 32.3563 15.8108 32.2571 12.6064 30.9193C9.40192 29.5815 6.80902 27.1006 5.33117 23.9582C3.85332 20.8159 3.596 17.2365 4.60918 13.9152C5.62236 10.5938 7.83373 7.76745 10.8139 5.98504C13.794 4.20263 17.3302 3.59134 20.7357 4.26989C24.1412 4.94844 27.1731 6.86839 29.2425 9.6569" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "Stickies & Markers":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path d="M22.5 29.5833C22.5 29.5833 21.0833 31 18.25 31L19.8025 29.1406L20.0347 21.8833L22.5 19.9674H27.9597L31 18.25C31 21.3194 29.5833 22.5 29.5833 22.5L22.5 29.5833Z" fill="white"/>\n<path d="M8.5 5L27.5 5C29.433 5 31 6.567 31 8.5V16.6111C31 18.5441 29.433 20.1111 27.5 20.1111H22.6111C21.2304 20.1111 20.1111 21.2304 20.1111 22.6111V27.5C20.1111 29.433 18.5441 31 16.6111 31H8.5C6.567 31 5 29.433 5 27.5L5 8.5C5 6.567 6.567 5 8.5 5Z" stroke="black" stroke-width="2"/>\n<path d="M31 14V18.25C31 21.3194 29.5833 22.5 29.5833 22.5L22.5 29.5833C22.5 29.5833 21.0833 31 18.25 31H14" stroke="black" stroke-width="2"/>\n<rect x="9" y="10" width="15" height="2" rx="1" fill="black"/>\n<rect x="9" y="14" width="7" height="2" rx="1" fill="black"/>\n</svg>\n',
  "Index Cards":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<path fill-rule="evenodd" clip-rule="evenodd" d="M5 22V13H22V17.5385L24 15.5677V12.5C24 11.6716 23.3284 11 22.5 11H4.5C3.67157 11 3 11.6716 3 12.5V22.5C3 23.3284 3.67157 24 4.5 24H15.4427L17.4724 22H5Z" fill="black"/>\n<path fill-rule="evenodd" clip-rule="evenodd" d="M11.689 8.99998L12.9083 4.44947L29.329 8.8494L28.4956 11.9596C29.1185 11.8379 29.8151 11.8152 30.5623 11.974L31.3903 8.88407C31.6047 8.08387 31.1298 7.26137 30.3296 7.04696L12.9429 2.38821C12.1427 2.1738 11.3202 2.64867 11.1058 3.44887L9.61841 8.99998H11.689Z" fill="black"/>\n<rect x="7.5" y="15.5" width="8" height="1" rx="0.5" stroke="black"/>\n<path d="M16.0439 26.3672L27.7263 14.6839C28.7753 13.6349 30.6535 13.8124 31.9214 15.0805C33.1894 16.3485 33.3638 18.2238 32.3148 19.2728L20.6325 30.9561C20.6044 30.9842 20.5662 31 20.5264 31L16.15 31C16.0672 31 16 30.9328 16 30.85V26.4732C16 26.4334 16.0158 26.3953 16.0439 26.3672Z" fill="white" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M23.6658 27.9757C22.8 28.8415 20.8439 28.7227 19.6006 27.4794C18.3573 26.236 18.2385 24.28 19.1043 23.4142" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
  "Prepared Materials":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<g clip-path="url(#clip0_21_1886)">\n<path d="M20.0295 18.0252C14.5533 24.049 15.6659 31.298 15.6659 31.298L21.2569 24.9797L30.8142 26.2979L26.7754 17.6513L30.8142 10.0705C30.8142 10.0705 25.5057 12.0014 20.0295 18.0252Z" fill="white"/>\n<path d="M4.96627 19.142C3.71157 18.6381 3.71157 16.8619 4.96627 16.358L11.7357 13.6396C12.27 13.4251 12.6343 12.9236 12.6733 12.3492L13.1668 5.07104C13.2582 3.72204 14.9475 3.17316 15.8144 4.21076L20.4916 9.80881C20.8608 10.2507 21.4503 10.4422 22.0087 10.3017L29.0831 8.52201C30.3943 8.19214 31.4384 9.62913 30.7194 10.7742L26.8407 16.9524C26.5346 17.4401 26.5346 18.0599 26.8407 18.5476L30.7194 24.7258C31.4384 25.8709 30.3943 27.3079 29.0831 26.978L22.0087 25.1983C21.4503 25.0578 20.8608 25.2493 20.4916 25.6912L15.8144 31.2892C14.9475 32.3268 13.2582 31.778 13.1668 30.429L12.6733 23.1508C12.6343 22.5764 12.27 22.0749 11.7357 21.8604L4.96627 19.142Z" stroke="black" stroke-width="2"/>\n</g>\n<defs>\n<clipPath id="clip0_21_1886">\n<rect width="36" height="36" fill="white"/>\n</clipPath>\n</defs>\n</svg>\n',
  "No Supplies":
    '<svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">\n<circle cx="18" cy="18" r="14" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M24 12L12 24" stroke="black" stroke-width="2" stroke-linecap="round"/>\n<path d="M24 24L12 12" stroke="black" stroke-width="2" stroke-linecap="round"/>\n</svg>\n',
};

if (false) {
  // If you change the icons, you will need to run this code, console log a new object, and copy paste it above.

  // Focus
  Object.keys(IconURLs).forEach(async (icon, index) => {
    let promise = await fetch(IconURLs[icon]);
    let svgString = await promise.text();
    SVGStrings[icon] = svgString;
  });

  // Group Size
  Object.keys(GroupSizeURLs).forEach(async (icon) => {
    let promise = await fetch(GroupSizeURLs[icon]);
    let svgString = await promise.text();
    SVGStrings[icon] = svgString;
  });

  // Durations
  Object.keys(DurationURLs).forEach(async (icon) => {
    let promise = await fetch(DurationURLs[icon]);
    let svgString = await promise.text();
    SVGStrings[icon] = svgString;
  });

  // Props
  Object.keys(dataPropUrls).forEach(async (icon) => {
    let promise = await fetch(dataPropUrls[icon]);
    let svgString = await promise.text();
    SVGStrings[icon] = svgString;
  });

  console.log(SVGStrings);
}

const generateNav = (index) => {
  _("#prevButton").onclick = () => {
    let target = index == 0 ? window.recordCount - 1 : parseInt(index) - 1;

    history.pushState(
      {
        urlPath: "?cardID=" + target,
      },
      "Card " + target,
      "?cardID=" + target,
    );
    renderPDF(target, false);
  };

  _("#nextButton").onclick = () => {
    let target = index == window.recordCount - 1 ? 0 : parseInt(index) + 1;

    history.pushState(
      {
        urlPath: "?cardID=" + target,
      },
      "Card " + target,
      "?cardID=" + target,
    );
    renderPDF(target, false);
  };
};

async function renderPDF(index) {
  let data = window.cardObjects[index];
  generateNav(index);

  let layout = _("#layout").value;
  let paper;

  const isInkSaver = _("#inksaver").checked;

  if (layout == "4") {
    // letter size
    paper = {
      width: i(8.5),
      height: i(11),
    };
  } else if (layout == "1") {
    paper = {
      width: bleed.width,
      height: bleed.height,
    };
  } else {
    paper = {
      width: card.width,
      height: card.height,
    };
  }

  let doc = new PDFDocument({
    size: [paper.width, paper.height],
  });
  let stream = doc.pipe(blobStream());

  const poppins = await fetch("font/Poppins-Regular.ttf");
  const poppinsBuffer = await poppins.arrayBuffer();
  doc.registerFont("Poppins", poppinsBuffer);

  const poppinsBold = await fetch("font/Poppins-SemiBold.ttf");
  const poppinsBoldBuffer = await poppinsBold.arrayBuffer();
  doc.registerFont("PoppinsBold", poppinsBoldBuffer);

  const lato = await fetch("font/Lato-Regular.ttf");
  const latoBuffer = await lato.arrayBuffer();
  doc.registerFont("Lato", latoBuffer);

  const latoBold = await fetch("font/Lato-Bold.ttf");
  const latoBoldBuffer = await latoBold.arrayBuffer();
  doc.registerFont("LatoBold", latoBoldBuffer);

  window.fontTitle = "PoppinsBold";
  window.fontBody = "Lato";
  window.fontBodyBold = "LatoBold";

  _("#title").innerHTML =
    "<strong>" +
    data.title +
    "</strong> (" +
    index +
    "/" +
    window.cardObjects.length +
    ")";

  // Added if because of the special card
  if (layout == "4") {
    await renderCard(i(1), i(1), doc, data, index, isInkSaver, true);
    await renderBack(i(4), i(1), doc, data, index);

    let nextCard = (index + 1) % window.cardObjects.length;
    await renderCard(
      i(1),
      i(5),
      doc,
      window.cardObjects[nextCard],
      nextCard,
      isInkSaver,
      true,
    );
    await renderBack(i(4), i(5), doc, window.cardObjects[nextCard], nextCard);
  } else if (layout == "0") {
    await renderCard(0, 0, doc, data, index, isInkSaver, false);
    doc.addPage();
    await renderBack(0, 0, doc, data, index);
  } else {
    await renderCard(
      bleedMargin,
      bleedMargin,
      doc,
      data,
      index,
      isInkSaver,
      true,
    );
    doc.addPage();
    await renderBack(bleedMargin, bleedMargin, doc, data, index);
  }

  doc.end();

  stream.on("finish", () => {
    const url = stream.toBlobURL("application/pdf");

    if (layout == "4") {
      _("#pdfView").src = url + "#zoom=page-width";
    } else {
      _("#pdfView").src = url + "#zoom=page-fit";
    }
  });
}

async function generateKVS() {
  window.KeyValues = {};

  Store.eachPage(
    function page(kvs, fetchNextPage) {
      try {
        kvs.forEach((pair) => {
          let value = pair.get("Value");
          if (parseFloat(value)) {
            value = parseFloat(value);
          }

          window.KeyValues[pair.get("Key")] = value;
        });
      } catch (e) {
        console.log("error inside eachPage => ", e);
      }

      fetchNextPage();
    },
    function done(err) {
      if (err) {
        console.error(err);
        return;
      } else {
        document.body.style.setProperty(
          "--colorBlue",
          window.KeyValues["colorBlue"],
        );
        document.body.style.setProperty(
          "--colorGreen",
          window.KeyValues["colorGreen"],
        );
        document.body.style.setProperty(
          "--colorYellow",
          window.KeyValues["colorYellow"],
        );
        document.body.style.setProperty(
          "--colorRed",
          window.KeyValues["colorRed"],
        );
      }
    },
  );
}

generateKVS();

window.onload = () => {
  window.cardObjects = [];
  window.recordCount = 0;

  let specialCard = {
    title: "Special",
    explanation: "special",
    focus: "special",
    group: "special",
    props: "special",
    time: "special",
    shortlink: "special",
  };

  window.cardObjects.push(specialCard);

  let button = document.createElement("button");
  button.style.setProperty("--hoverColor", "moccasin");
  button.innerHTML = `<figure>⭐️</figure> Information Card`;
  // button.innerHTML = `<mark>${index}</mark> ${card.title}`;
  button.onclick = (e) => {
    history.pushState(
      {
        urlPath: "?cardID=0",
      },
      "Card 00",
      "?cardID=0",
    );
    renderPDF(0, false);
  };
  _("#cardList").append(button);

  Records.eachPage(
    function page(records, fetchNextPage) {
      try {
        records.forEach((record, index) => {
          window.recordCount++;

          // Because we added the special card in the front
          index = index + 1;

          if (record.get("Title") != undefined) {
            let card = {
              title: record.get("Title") || "<mark>invalid</mark>",
              explanation: record.get("Explanation") || "invalid",
              focus: record.get("Focus") || "<mark>invalid</mark>",
              group: record.get("Group") || "invalid",
              props: record.get("Props Set Up") || "invalid",
              time: record.get("Time") || "invalid",
              shortlink: record.get("Short link") || "invalid",
            };

            window.cardObjects.push(card);

            let button = document.createElement("button");
            button.style.setProperty(
              "--hoverColor",
              window.KeyValues[Colors[card.focus]],
            );

            button.innerHTML = `<figure>${emoji[card.focus]}</figure> ${index} ${card.title}`;
            // button.innerHTML = `<mark>${index}</mark> ${card.title}`;

            button.onclick = (e) => {
              history.pushState(
                {
                  urlPath: "?cardID=" + index,
                },
                "Card " + index,
                "?cardID=" + index,
              );
              renderPDF(index, false);
            };
            _("#cardList").append(button);
          }
        });
      } catch (e) {
        console.log("error inside eachPage => ", e);
      }

      fetchNextPage();
    },
    function done(err) {
      if (err) {
        console.error(err);
        return;
      } else {
        let urlparams = new URLSearchParams(window.location.search).get(
          "cardID",
        );
        let selectedCard = urlparams ? urlparams : 0;
        renderPDF(selectedCard);
      }
    },
  );
};
